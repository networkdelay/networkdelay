package threads;

import se.lth.control.realtime.*;


public class Actuator extends Thread {
	private Monitor mon;
	private AnalogOut uChan;

	public Actuator(Monitor mon){
		this.mon = mon;
	}

	public void run(){
		try {
			uChan = new AnalogOut(0);
			System.out.println("Actuator connected");
		} catch (Exception e) {
			System.out.println("Unable to connect to actuator: ");
			e.printStackTrace();			
		}
		try {
			uChan.set(0.0);
		} catch (Exception e) {
			System.out.println(e);
		}
		while (true){
					double u = mon.getU();
					try {
						uChan.set(u);
					} catch (Exception e) {
						System.out.println(e);
					}				
		}

	}

}
