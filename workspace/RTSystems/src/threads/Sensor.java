package threads;

import se.lth.control.realtime.*;

public class Sensor extends Thread{
	private Monitor mon;
	private double period;
	private AnalogIn yChan;
	private double vel;

	public Sensor(Monitor mon, double period){
		this.mon = mon;
		this.period = period;
	}

	public void run(){
		long duration;
		long t = System.currentTimeMillis();

		try {
			try{
				yChan = new AnalogIn(0);
				System.out.println("Sensor connected");
			} catch(UnsatisfiedLinkError error) {
				System.out.println("No sensor present, please connect to process");
			}
		} catch(Exception e) {
			System.out.println("Unable to connect to sensor");
			e.printStackTrace();
		}



		while(true){
			try {
				vel = yChan.get();
			} catch (Exception e) {
				System.out.println("Unable to obtain velocity from AnalogIn.");
				e.printStackTrace();
			}
			mon.updateVel(vel);

			period = mon.getParameters().H;
			t = (long) (t + period*1000);
			duration = t - System.currentTimeMillis();
			if(duration > 0){
				try {
					Thread.sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
