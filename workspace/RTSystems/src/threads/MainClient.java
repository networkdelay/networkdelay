package threads;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
*Creates a Client object and does the following:
*	1. Sets up a connection to the given URL
*	2. Tries to send the double value of u as a byte array
*	3. Receives and prints u as a double
*	4. Destroys the connection to the socket
*	5. Tries to send to broken socket, should give IOException
*	6. Sets up a new connection to the new URL
*
*Left to do:
*	- Make sure byte array is sent in right order (appears to work anyways??)
*	- Read delay. How? Maybe shift order and read first bytes? or call on 
*	ByteBuffers.native() method to set correct order?
*	- Two kinds of byte order; BigEndian and LittleEndian. Java is Big wich means most
*	important bit first in the byte array.
*
*	Big: 	[A][B][C][1][2][3]
*	Small:	[3][2][1][C][B][A]
*
*/
public class MainClient {

	public static void main(String[] args) {
		System.out.println("Test program");
		//		Monitor mon = new Monitor(0.05);


		double u = 2.1234567891234567;
		byte[] bytes = new byte[8];

		//Packs value u to a byte array.
		ByteBuffer.wrap(bytes).putDouble(u);


		//Sets up a connection to socket
		Client c = new Client();
		c.setup("www.southafrica.info");
		System.out.println("Sends double value "+u+" as a byte array ");

		byte[] recieved = new byte[12];

		for(int i=0; i<10; i++){
			try {
				recieved = c.send(bytes);
			} catch (IOException e) {
				System.out.println("Sending failed");
				e.printStackTrace();
			}

			byte[] timeStamp = new byte[4];
			byte[] controlSignal = new byte[8];

			for(int j=0; j<4; j++){
				timeStamp[4-1-j] = recieved[j];
			}
			for(int k=0; k<Double.BYTES; k++){
				controlSignal[k] = recieved[4+k];
			}

			int ts = ByteBuffer.wrap(timeStamp).getInt();
			double cs = ByteBuffer.wrap(controlSignal).getDouble();
			double tsd = (double) ts; 

			System.out.println("Control signal returned "+ cs);
			System.out.println("Time stamp returned "+ ts/1000.0 + " ms");
		}
		c.closePingserver();


		//Tries to send to a closed socket
		byte[] recieved2 = new byte[10];
		try {
			recieved2 = c.send(bytes);
		} catch (IOException e) {
			System.out.println("Sending failed");
			e.printStackTrace();
		}
		u = ByteBuffer.wrap(recieved).getDouble();
		System.out.println("Value returned "+u);


		//Starts new pingserver with new adress (URL/IP)
		c.setup("www.google.com");
		System.out.println("Sends double value "+u+" as a byte array ");

		byte[] recieved3 = new byte[10];
		try {
			recieved3 = c.send(bytes);
		} catch (IOException e) {
			System.out.println("Sending failed");
			e.printStackTrace();
		}	
		u = ByteBuffer.wrap(bytes).getDouble();
		System.out.println("Value returned "+u);

	}

}
