package threads;

import java.io.*;
import org.lirc.socket.*;

/**
 * A client responsible for setting up connection and send ping messages 
 * to a server located at a given URL or IP-address. Uses pre-written UNIX sockets 
 * under GNU-license.
 */

public class Client {

	private UnixSocket sock;
	private OutputStream os;
	private InputStream is;
	private Process p;
	private Runtime runTime;
	private Boolean connected;

	/**
	 * Starts a new Client that is ready to setup new pingserver with call to
	 * setup.
	 */
	public Client() {
		
		connected = false;
		runTime = Runtime.getRuntime();
	}

	/**
	 * Sets up connection to given IP-address or URL. Starts external program
	 * pingserver, waits for 2 seconds before setting up socket, input- and
	 * outputstream. Called from monitor.
	 * 
	 * @param url URL or IP-address of server to ping.
	 * @return true if connection was set
	 */
	public boolean setup(String url) {
		System.out.println("Setting up connection to " + url + "...");
		StringBuilder sb = new StringBuilder();
		sb.append("pingserver ");
		sb.append(url);
		String command = sb.toString();
		
		//If socket already connected, destroy connection before starting new.
		if(connected){
			System.out.println("Closing old server...");
			closePingserver();
		}
		//Starting new pingserver at URL by calling the JVM:s runtime object
		//and creating a process which calls "pingserver" + url.		
		try {
			p = runTime.exec(command);
			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		//Sleeps for two seconds before trying to setup UnixSocket connection
		//to file pingsocket located in folder /tmp created by pingserver.
		try {
			Thread.sleep(2000);
			sock = new UnixSocket("/tmp/pingsocket");
		} catch (IOException e) {
			System.out.println("Failed to setup socket:");
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Failed to setup socket:");
			return false;
		}

		//Setup input and output strprintStackTraceeam to socket
		try {
			os = sock.getOutputStream();
			is = sock.getInputStream();
		} catch (IOException e) {
			System.out.println("Failed to setup input and output stream: ");
			e.printStackTrace();
			return false;
		}
		connected = true;
		return true;
	}
	/**
	* Sends byte b to pingserver.
	* @returns recieved byte with size of Double.SIZE+Long.SIZE
	* @throws IOException()
	*/
	public byte[] send(byte[] b) throws IOException {
		
		if(!connected){
			throw new IOException("Attempt is made to invoke an IO operation upon a socket channel that is not yet connected.");		
		}
		int read=0;
		byte[] recieved = new byte[Double.SIZE+Long.SIZE];

		try {
			os.write(b);
		} catch (IOException e) {
			System.out.println("Failure to send: ");
			e.printStackTrace();
		}
		try {
			read= is.read(recieved);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return recieved;
	}

	/**
	 * Closing pingserver. Must be done before starting a new.
	 * 
	 * @return true if succeeded in shutting down else false
	 */
	public boolean closePingserver() {
		runTime = Runtime.getRuntime();
		try {
			p.destroy();
			p.waitFor();	//Might be waiting for process to be destroyed?
		} catch (InterruptedException e) {
			System.out.println("Failed to terminate process: pingserver");
			e.printStackTrace();		
		}
		System.out.println("Pingserver terminated");
		connected = false;
		return true;
	}
}
