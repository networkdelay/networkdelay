package threads;

import threads.gui.Opcom;

public class Main {


	public static void main(String[] args) {
		double sampleTime=0.05; 	// [seconds]
		double maxDelay=0.2;		// [seconds]
		double minSampleTime=0.01; 	// [seconds]

		Monitor mon = new Monitor(sampleTime, minSampleTime, maxDelay);
		Client client = new Client();
		Opcom op = new Opcom(mon, maxDelay);
		mon.setVelRef(0);
		mon.updateDelay(0.0);
		Controller con = new Controller(mon, mon.getParameters().H, client);
		con.setPriority(9);
		Actuator act = new Actuator (mon);
		act.setPriority(8);
		Sensor sen = new Sensor(mon, mon.getParameters().H);
		sen.setPriority(8);
		op.initializeGUI("Smith Contoller", mon.getParameters());
		

		sen.start();
		con.start();
		act.start();
		op.start();

	}

}
