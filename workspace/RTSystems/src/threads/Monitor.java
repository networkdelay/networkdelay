package threads;

public class Monitor implements Cloneable{

	/** Actuator-attributes */


	/** Modes */
	public final static int OLD=0;	// Ordinary local delay
	public final static int SID=1;	// Static internet delay
	public final static int DID=2;	// Dynamic internet delay
	private int mode;				// Mode set in User-GUI
	
	/** Controller attributes*/
	private double vel;
	private double velRef;			// Velocity Reference value set in User-GUI
	private PIParameters p;
	private double u;
	private double yHat;
	private double delay;
	private double realDelay;
	private boolean uChanged;
	
	/** RingBuffer attributes*/
	private double[] ringBuffer;	
	private int put;				// pointer in ringbuffer
	private int get;				// pointer in ringbuffer
	private boolean first;
	private double[] ringBufferYHat;// Ringbuffer for the simulated process calculation
	private int getYHat;			// pointer in ringbuffer
	private int putYHat;			// pointer in ringbuffer
	private boolean firstYHat;
	private double yHatDelayed;
	
	/**Internet-delay attributes*/
	private boolean serverConnected=false;// For gui to know if server connection was successful  
	private boolean serverFlag=false;
	private boolean urlFlag=false;
		
	/** GUI-attributes */
	private String ip;
	private boolean didFlag;
		

	/** Constructor */
	public Monitor(double sampleTime, double minSampleTime, double maxDelay){
		p = new PIParameters();
		p.H= sampleTime;
		p.integratorOn = true;
		p.K=2.6133;
		p.Ti=10.445;
		p.Tr=1;
		
		mode = Monitor.OLD;													// Start mode
		ringBuffer = new double[(int) ((maxDelay/minSampleTime)+1)];
		get = 0;
		put = 0;
		first = true;
		ringBufferYHat = new double[(int) ((maxDelay/minSampleTime)+1)];
		getYHat = 0;
		putYHat = 0;
		firstYHat = true;
		yHat = 0;
		yHatDelayed = 0;
		
		didFlag = false;
		
	}

	/** Called by Actuator & Plotter-GUI to get u put by the controller */
	public synchronized double getU(){
		while(!uChanged){
			try {
				wait();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		uChanged = false;
		return u;
	}

	/** Called by the sensor to update the velocity and notify the controller*/
	public synchronized void updateVel(double vel){
		this.vel = vel;
		notifyAll();
	}

	/** Called by controller to get mode put by User-GUI */
	public synchronized int getMode(){
		return mode;
	}

	/** Called by controller and the GUI to reset the delay (DID mode)*/
	public synchronized void resetDelay(){
		delay = 0.0;
		realDelay=0.0;
	}

	/** Called by the controller and the GUI to get the delay */
	public synchronized double getDelay(){
		if(mode == Monitor.OLD) {
			return 0;
		} else {
			return delay;
		}
	}
	
	/** Called by the controller and the GUI to get the delay */
	public synchronized double getRealDelay(){
		if(mode == Monitor.DID) {
			return realDelay;
		} else {
			return delay;
		}
	}

	/** Called by controller to update the delay (DID and SID mode)*/
	public synchronized void updateDelay(double delay){
		this.delay=delay;
		if(mode == Monitor.SID){
			put = 0;
			get = 0;
			putYHat = 0;
			getYHat = 0;
			first = true;
		}
	}
	
	/** Called by controller to update the real delay (DID)*/
	public synchronized void updateRealDelay(double realDelay){
		this.realDelay=realDelay;
	}

	/** Called by controller to update the control signal for GUI and the Actuator*/
	public synchronized void updateU(double u){
		if(mode == Monitor.SID){
			ringBuffer[put]=u;
			put ++;
			if(put>ringBuffer.length-1){
				put = 0;
				first = false;
			}

			if(put>=delay/p.H || !first){
				this.u=ringBuffer[get];
				uChanged = true;
				notifyAll();
				get++;
				if(get>ringBuffer.length-1){
					get=0;
				}
			}
		}else{
			this.u=u;
			uChanged = true;
			notifyAll();
		}

	}

	/**Called by the controller to get the estimated velocity*/
	public synchronized double getYHat(){
			return ringBufferYHat[putYHat];
	}

	/**Called by the controller to update the model and to get the delayed estimated velocity*/
	public synchronized double getYHatDelayed(double u){

		yHat = yHat*p.H-0.12*yHat+2.25*u*p.H; //FORWARD DIFFERENCE
		putYHat ++;
		if(putYHat>ringBufferYHat.length-1){
			putYHat = 0;
			firstYHat = false;
		}
		ringBufferYHat[putYHat]=yHat;
			
		if(putYHat>=delay/p.H || !firstYHat){
			yHatDelayed=ringBufferYHat[getYHat];
			uChanged = true;
			notifyAll();
			getYHat++;
			if(getYHat>ringBufferYHat.length-1){
				getYHat=0;
			}
		}
		return yHatDelayed;
	}
	
	/** Called by controller to set server online */
	public synchronized void serverStatus(boolean online){
		serverConnected = online;
		serverFlag = true;
		notifyAll();
	}

	/** User-GUI specific methods */
	/** Called by user to change controller parameters */
	public synchronized void updateParameters(PIParameters params){							
		p=params;
	}

	/** Called by user to change controller mode (OLD=0, SID=1, DID=2) */
	public synchronized void updateControllerMode(int mode){			
		switch(mode){
		default: break;
		case 0: {this.mode = OLD; setDidFlag(false); break;}
		case 1: {this.mode = SID; setDidFlag(false); break;}
		case 2: {this.mode = DID; break;}
		}
	}
	
	/** Own method for the GUI to get U because the getU method is one time exclusive */
	public synchronized double guiGetU(){
		return u;
	}

	/** To set the Pingserver ULR or ip from the GUI to */
	public synchronized void setURL(String adress){
		ip=adress;
		urlFlag=true;
		notifyAll();
	}
	
	/** Returns the current set Pingserver URL or ip. Called by GUI. */
	public synchronized String getURL(){
		while(!urlFlag){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		urlFlag=false;
		return ip;

	}
	
	/** Called by gui. Returns if the pingserver is online or not */
	public synchronized boolean isOnline(){
		while(!serverFlag){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		serverFlag=false;
		return serverConnected;
	}
	
	/** Called by all. Returns if the pingserver is online or not */
	public synchronized boolean isServerConnected(){
		return serverConnected;
	}
	
	/** Called by the controller to clone PI parameters*/
	public synchronized PIParameters getParameters(){
		return (PIParameters) p.clone();
	}

	/** Called by the controller to get the measured velocity*/
	public synchronized double getVel(){	
		return vel;
	}

	/** Called by the controller and the GUI to get the reference value for the velocity*/
	public synchronized double getVelRef(){	
		return this.velRef;
	}
	/** Called by the GUI to update the velocity reference */
	public synchronized void setVelRef(double ref){
		this.velRef = ref;
		notifyAll();
	}
	
	/**Called by GUI and Controller to set the DidFlag*/
	public synchronized void setDidFlag(boolean didFlag){
		this.didFlag = didFlag;
	}
	
	/**Called by Controller to get the DidFlag*/
	public synchronized boolean didFlag(){
		return didFlag;
	}



}
