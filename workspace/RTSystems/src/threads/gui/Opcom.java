package threads.gui;

import javax.swing.*;


import java.awt.*;
import java.awt.event.*;
import se.lth.control.*;
import threads.*;
import se.lth.control.plot.PlotterPanel;
import se.lth.control.realtime.AnalogIn;
import se.lth.cs.realtime.*;
import threads.Monitor;


class PlotData implements Cloneable {
	double ref, y; 
	double x; // holds the current time 

	public Object clone() {
		try { 
			return super.clone(); 
		} catch (Exception e) {return null;} 
	} 
} 

class Reader extends Thread {
	private Opcom opcom;
	private Monitor mon;

	AnalogIn velChan, posChan, ctrlChan, delayChan;

	/** Constructor. Sets initial values of the controller parameters and initial mode. */
	public Reader(Opcom opcom, Monitor m) {

		this.opcom = opcom;
		mon=m;
	}

	/** Run method. Sends data periodically to Opcom. */
	public void run() {
		final long h = 25; // period (ms)
		long duration;
		long t = System.currentTimeMillis();
		DoublePoint dp;
		PlotData pd;
		DoublePoint delayData;
		double vel = 0, ref = 0, ctrl = 0, delay=0;
		double realTime = 0;

		setPriority(7);

		while (true) {
			try {
				ref = mon.getVelRef();
				vel = mon.getVel();
				ctrl = mon.guiGetU();
				delay = mon.getRealDelay()*1000;			// returned in seconds by Monitor

			} catch (Exception e) {
				System.out.println(e);
			} 

			pd = new PlotData();
			pd.y = vel;
			pd.ref = ref;
			pd.x = realTime;
			opcom.putMeasurementDataPoint(pd);

			dp = new DoublePoint(realTime,ctrl);
			opcom.putControlDataPoint(dp);

			delayData = new DoublePoint(realTime,delay);
			opcom.putDelayDataPoint(delayData);
			realTime += ((double) h)/1000.0;

			t += h;
			duration = (int) (t - System.currentTimeMillis());
			if (duration > 0) {
				try {
					sleep(duration);
				} catch (Exception e) {}
			}
		}
	}
}

/** Class that creates and maintains a GUI for the Ball and Beam process. 
	 Uses two internal threads to update plotters */

public class Opcom{    
	private final int WIDTH=250;
	private final int HEIGHT=WIDTH*3/4;
	private final int SCALE=3;

	private Monitor mon;
	private PIParameters params;
	private Reader reader;

	private double velRef; 
	private double maxDelay;
	private double sid;

	private String did;

	// Background frame
	private JFrame frame;

	// Background panels
	private JPanel opcomPane = new JPanel();
	private JSplitPane splitPane;
	private JPanel modePane = new JPanel();
	private JPanel paramPane = new JPanel();

	private JPanel paramsLabelPanel = new JPanel();
	private JPanel paramsFieldPanel = new JPanel();

	// Reference settings
	private JPanel refPane = new JPanel();
	private DoubleField refField = new DoubleField(6,3);

	private JLabel refText = new JLabel("Ref:");
	private JButton refButton = new JButton("Apply");

	// Parameter settings 
	private BoxPanel paramsPanel = new BoxPanel(BoxPanel.HORIZONTAL);
	private DoubleField paramsKField = new DoubleField(6,3);
	private DoubleField paramsTiField = new DoubleField(6,3);
	private DoubleField paramsTrField = new DoubleField(6,3);
	private DoubleField paramsHField = new DoubleField(6,3);
	private JButton paramsButton = new JButton("Apply");


	// Mode settings
	private JPanel mode1 = new JPanel();
	private JPanel mode2 = new JPanel();
	private JPanel mode3 = new JPanel();

	private JPanel split1 = new JPanel();
	private JPanel split2 = new JPanel();
	private JPanel split3 = new JPanel();

	private JButton set2 = new JButton("Set delay");
	private JButton set3= new JButton("Set URL");
	private JButton setReset= new JButton("Reset delay");

	private JRadioButton radio1 = new JRadioButton();
	private JRadioButton radio2 = new JRadioButton();
	private JRadioButton radio3 = new JRadioButton();

	private JLabel info1 = new JLabel();
	private JLabel info2 = new JLabel();
	private JLabel info3 = new JLabel();

	// Plotter panel settings
	private BoxPanel plotterPanel;

	private double range = 10.0; // Range of time axis
	private int divTicks = 5;    // Number of ticks on time axis
	private int divGrid = 5;     // Number of grids on time axis

	private boolean hChanged = false; 

	// Plotters
	private PlotterPanel measurementPlotter; // has internal thread
	private PlotterPanel controlPlotter; // has internal thread
	private PlotterPanel delayPlotter; // has internal thread

	/** Constructor. Creates the plotter panels. */
	public Opcom(Monitor m, double maxDelayInSeconds) {
		mon = m;
		reader = new Reader(this, mon);
		measurementPlotter = new PlotterPanel(2, 4); 	// Two channels
		controlPlotter = new PlotterPanel(1, 4);		// One channel
		delayPlotter = new PlotterPanel(1, 4);			// One channel
		maxDelay=maxDelayInSeconds*1000;				// Converted to milliseconds
		velRef=0.0;
		radio1.setSelected(true);
		info1.setText("On local with no delay.");
	}

	/** Starts the threads. */
	public void start() {
		measurementPlotter.start();
		controlPlotter.start();
		delayPlotter.start();
		reader.start();
	}

	/** Stops the threads. */
	public void stopThread() {
		measurementPlotter.stopThread();
		controlPlotter.stopThread();
		delayPlotter.stopThread();
	}


	/** Creates the GUI. Called from Main. */
	public void initializeGUI(String name, PIParameters PIParams) {
		params=PIParams;

		// New background frame
		frame = new JFrame(name);

		// Reference Settings
		refPane.setMaximumSize(new Dimension(WIDTH*SCALE/16, HEIGHT*SCALE/16));
		refPane.setLayout(new GridLayout(6,2));
		refText.setPreferredSize(new Dimension(10,5));
		refText.setMaximumSize(new Dimension(10,5));

		refField.setPreferredSize(new Dimension(10,5));
		refField.setMaximumSize(new Dimension(10,5));
		refButton.setPreferredSize(new Dimension(10,5));
		refButton.setMaximumSize(new Dimension(10,5));
		refPane.setBorder(BorderFactory.createTitledBorder("Velocity reference"));
		refButton.setEnabled(false);

		refPane.add(refText, "5,1");
		refPane.add(refField, "5,2");
		refPane.add(refButton, "6,1");
		refField.setValue(mon.getVelRef());
		refField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				double tempValue = refField.getValue();
				velRef = tempValue;
				if(velRef>10){
					velRef = 10;
					refField.setValue(10.0);
				}
				if(velRef<-10){
					velRef = -10;
					refField.setValue(-10.0);
				}
				refButton.setEnabled(true);
			}
		});
		refButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mon.setVelRef(velRef);
				refButton.setEnabled(false);}
		});


		// Mode panel settings
		modePane.setMinimumSize(new Dimension(WIDTH*SCALE*2/6, HEIGHT*SCALE*2/6));
		modePane.setMaximumSize(new Dimension(WIDTH*SCALE*2/3, HEIGHT*SCALE*2/3));

		split1.setLayout(new GridLayout(1, 0, (WIDTH*SCALE/3)-5, 0));
		split2.setLayout(new GridLayout(1, 0, (WIDTH*SCALE/3)-25, 0));
		split3.setLayout(new GridLayout(1, 0, 0, 0)); 	//(WIDTH*SCALE/3)-45

		set2.setEnabled(false);
		set3.setEnabled(false);	
		setReset.setEnabled(false);

		split1.add(radio1, "1");

		split2.add(radio2, "1");
		split2.add(set2, "2");

		split3.add(radio3, "1");
		split3.add(setReset, "2");
		split3.add(set3, "3");

		radio1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r1) {

				radio1.setEnabled(false);
				radio2.setEnabled(true);
				radio3.setEnabled(true);

				set2.setEnabled(false);
				set3.setEnabled(false);
				setReset.setEnabled(false);	


				info3.setText("");
				info3.repaint();

				radio3.setText("");
				radio3.repaint();

				radio2.setSelected(false);
				radio3.setSelected(false);

				info1.setText("On local with no delay.");
				info1.repaint();
				info2.setText("");
				info2.repaint();
				info3.setText("");
				info3.repaint();

				radio3.setText("");
				radio3.repaint();

				mon.updateControllerMode(0);			// Set mode = 0
				mon.resetDelay();						// Set delay to 0
			}
		});

		radio2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r2) {
				radio1.setEnabled(true);
				radio2.setEnabled(false);
				radio3.setEnabled(true);

				set2.setEnabled(true);
				set3.setEnabled(false);
				setReset.setEnabled(false);

				info3.setText("");
				info3.repaint();

				radio1.setSelected(false);
				radio3.setSelected(false);

				mon.updateControllerMode(1);			// Set mode = 1
				// Set pingServer OFF


				info1.setText("");
				info1.repaint();
				info3.setText("");
				info3.repaint();

				sid=0.0;


			}
		});
		set2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent s2) {
				String temp;
				try{
					temp = JOptionPane.showInputDialog("Input a static delay [ms]");
					if(temp!=null){
						sid = Double.parseDouble(temp);
					}
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(frame, "Input must be numbers only");
					// Make sure that only figures are allowed
					return;
				}
				if(temp!=null){
					if(sid>maxDelay){sid=maxDelay;} else if(sid<0.0){sid=0.0;}

					info2.setText("Static internet delay is set to " + sid + " ms");
					info2.repaint();
					set2.setEnabled(true);
					mon.updateDelay(sid/1000);
				}
			}});

		String html1 = "<html><body style='width: ";
		String html2 = "px'>";
		int help = WIDTH*SCALE/4;

		radio3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r3) {

				radio1.setEnabled(true);
				radio2.setEnabled(true);
				radio3.setEnabled(false);

				set2.setEnabled(false);
				set3.setEnabled(true);
				setReset.setEnabled(true);

				radio1.setSelected(false);
				radio2.setSelected(false);



				info1.setText("");
				info1.repaint();
				info2.setText("");
				info2.repaint();
				mon.updateControllerMode(2);
			}
		});
		set3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent s3) {
				did = JOptionPane.showInputDialog("Please input a URL or IP-adress");
				if(did != null){
					mon.setURL(did);
					mon.setDidFlag(true);
					if(mon.isOnline()){
						info3.setText(html1 + help + html2 + "Ping server on " + did);
						info3.repaint();
						set3.setEnabled(true);
						System.out.println("Server ONLINE");
					} else {
						info3.setText(html1 + help + html2 + "Unable to connect. Please retry.");
						info3.repaint();
						set3.setEnabled(true);
						System.out.println("Server OFFLINE");
					}
				}

			}});

		setReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent rs) {
				if(mon.isServerConnected()){
					info3.setText("The accumulated estimated delay was reset");
					info3.repaint();
				} else {
					info3.setText(html1 + help + html2 + "Pingserver is not connected");
					info3.repaint();
				}
				setReset.setEnabled(true);
				mon.resetDelay();
			}});


		mode1.setLayout(new GridLayout(0,1));
		mode1.setBorder(BorderFactory.createTitledBorder("Ordinary Local Delay"));
		mode1.add(split1, "1");
		mode1.add(info1, "2");

		mode2.setLayout(new GridLayout(0,1));
		mode2.setBorder(BorderFactory.createTitledBorder("Static Internet Delay"));
		mode2.add(split2, "1");
		mode2.add(info2, "2");

		mode3.setLayout(new GridLayout(0,1));
		mode3.setBorder(BorderFactory.createTitledBorder("Dynamic Internet Delay"));
		mode3.add(split3, "1");
		mode3.add(info3, "2");

		mode1.setSize(WIDTH*SCALE/6, HEIGHT*SCALE/6);
		mode2.setSize(WIDTH*SCALE/6, HEIGHT*SCALE/6);
		mode3.setSize(WIDTH*SCALE/6, HEIGHT*SCALE/6);

		modePane.setLayout(new GridLayout(0,1));
		modePane.add(mode1, "1");
		modePane.add(mode2, "2");
		modePane.add(mode3, "3");

		// Plotter settings
		opcomPane.setMinimumSize(new Dimension(WIDTH*SCALE*2/6, HEIGHT*SCALE*2/6));
		opcomPane.setMaximumSize(new Dimension(WIDTH*SCALE*2/3, HEIGHT*SCALE*2/3));

		// Plotter settings
		plotterPanel = new BoxPanel(BoxPanel.VERTICAL);
		plotterPanel.setBorder(BorderFactory.createTitledBorder("Plotter"));
		// Create plot components and axes, add to plotterPanel.
		measurementPlotter.setYAxis(20, -10, 4, 4);
		measurementPlotter.setXAxis(range, divTicks, divGrid);
		measurementPlotter.setTitle("Velocity (V)");
		plotterPanel.add(measurementPlotter);
		plotterPanel.addFixed(10);
		controlPlotter.setYAxis(20, -10, 4, 4);
		controlPlotter.setXAxis(range, divTicks, divGrid);
		controlPlotter.setTitle("Control (V)");
		plotterPanel.add(controlPlotter);

		delayPlotter.setYAxis((int)(maxDelay*1.5), -10, 5, 4);
		delayPlotter.setXAxis(range, divTicks, divGrid);
		delayPlotter.setTitle("Delay (ms)");
		plotterPanel.add(delayPlotter);

		opcomPane.add(plotterPanel, BorderLayout.CENTER);

		// Parameter settings
		paramPane.setLayout(new GridLayout(1,0));

		paramPane.setMinimumSize(new Dimension(WIDTH*SCALE/6, HEIGHT*SCALE/6));
		paramPane.setMaximumSize(new Dimension(WIDTH*SCALE/2, HEIGHT*SCALE/2));

		paramsLabelPanel.setLayout(new GridLayout(0,1));
		paramsLabelPanel.add(new JLabel("K: "));
		paramsLabelPanel.add(new JLabel("Ti: "));
		paramsLabelPanel.add(new JLabel("Tr: "));
		paramsLabelPanel.add(new JLabel("h: "));

		paramsFieldPanel.setLayout(new GridLayout(0,1));
		paramsFieldPanel.add(paramsKField); 
		paramsFieldPanel.add(paramsTiField);
		paramsFieldPanel.add(paramsTrField);
		paramsFieldPanel.add(paramsHField);

		paramsPanel.add(paramsLabelPanel);
		paramsPanel.addGlue();
		paramsPanel.add(paramsFieldPanel);
		paramsPanel.addFixed(WIDTH*SCALE/4);

		paramsKField.setValue(PIParams.K);
		paramsTiField.setValue(PIParams.Ti);
		paramsTrField.setValue(PIParams.Tr);
		paramsHField.setValue(PIParams.H);
		paramsKField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				double tempValue = paramsKField.getValue();
				params.K = tempValue;
				paramsButton.setEnabled(true);
			}
		});
		paramsTiField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double tempValue = paramsTiField.getValue();
				if (tempValue < 0.0) {
					paramsTiField.setValue(params.Ti);
				} else {
					params.Ti = tempValue;
					paramsButton.setEnabled(true);
					params.integratorOn = (params.Ti != 0.0);
				}
			}
		});
		paramsTrField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double tempValue = paramsTrField.getValue();
				if (tempValue < 0.0) {
					paramsTrField.setValue(params.Tr);
				} else {
					params.Tr = tempValue;
					paramsButton.setEnabled(true);
				}
			}
		});

		paramsHField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double tempValue = paramsHField.getValue();
				if (tempValue < 0.0) {
					paramsHField.setValue(params.H);
				} else {
					params.H = tempValue;
					hChanged=true;
					paramsButton.setEnabled(true);
				}
			} 
		});

		BoxPanel paramButtonPanel = new BoxPanel(BoxPanel.VERTICAL);
		paramButtonPanel.setBorder(BorderFactory.createTitledBorder("PI-parameters"));

		paramButtonPanel.addFixed(40);
		paramButtonPanel.add(paramsPanel);
		paramButtonPanel.addFixed(40);
		paramButtonPanel.add(paramsButton);

		paramsButton.setEnabled(false);
		paramsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mon.updateParameters(PIParams);			// sets parameters in monitor
				paramsButton.setEnabled(false);
			}
		});

		paramPane.add(paramButtonPanel, "1");
		paramPane.add(refPane, "2");

		// For Splitpanel (left field in gui) containing the paramPane on top and the modePane on the bottom
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, paramPane, modePane);
		splitPane.setMinimumSize(new Dimension(WIDTH*SCALE/6, HEIGHT*SCALE/6));
		splitPane.setMaximumSize(new Dimension(WIDTH*SCALE/3, HEIGHT*SCALE/3));
		splitPane.setDividerLocation(HEIGHT*SCALE/2);
		splitPane.setEnabled(false);

		// WindowListener that exits the system if the main window is closed.
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mon.setVelRef(0);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				stopThread();
				System.exit(0);
			}
		});

		// Frame settings
		frame.setLayout(new BorderLayout());

		frame.add(splitPane, BorderLayout.WEST);
		frame.add(opcomPane, BorderLayout.EAST);

		frame.setMinimumSize(new Dimension(WIDTH*SCALE/2, HEIGHT*SCALE/2));
		frame.setMaximumSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Pack the components of the window.
		frame.pack();

		// Position the main window at the screen center.
		Dimension sd = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension fd = frame.getSize();
		frame.setLocation((sd.width-fd.width)/2, (sd.height-fd.height)/2);

		// Make the window visible.
		frame.setVisible(true);
	}

	/** Called by Reader to put a control signal data point in the buffer. */
	public synchronized void putControlDataPoint(DoublePoint dp) {
		double x = dp.x;
		double y = dp.y;
		controlPlotter.putData(x, y);
	}

	/** Called by Reader to put a measurement data point in the buffer. */
	public synchronized void putMeasurementDataPoint(PlotData pd) {
		double x = pd.x;
		double ref = pd.ref;
		double y = pd.y;
		measurementPlotter.putData(x, ref, y);
	}    

	/** Called by Reader to put a delay signal data point in the buffer. */
	public synchronized void putDelayDataPoint(DoublePoint delayDp) {
		double x = delayDp.x;
		double y = delayDp.y;
		delayPlotter.putData(x, y);
	}
}
