package threads;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Controller extends Thread {
	private Monitor mon;
	private PIParameters pi;
	private double I;
	private double yref;
	private double y;
	private double yHat;
	private double u;

	private double period;
	private double ref2;
	private double yHatDelayed;
	private double usat;
	private Client client;
	private boolean keepConnecting;

	public Controller(Monitor mon, double period, Client client){
		this.mon = mon;
		this.period = period;
		this.client = client;
		keepConnecting = true;
	}

	public void run(){
		long duration;
		long t = System.currentTimeMillis();

		while(true){
			keepConnecting = true;
			while(keepConnecting && mon.didFlag()){
				if(client.setup(mon.getURL())){
					
					mon.serverStatus(true);
					mon.updateDelay(estimateDelay(10.0));
					mon.setDidFlag(false);
					keepConnecting = false;
				} else {
					System.out.println("else-sats");
					mon.serverStatus(false);
				}
			}
			
			double usignal = calculateOutput(mon.getVel(), mon.getVelRef());	
			if(mon.getMode() == Monitor.DID && mon.isServerConnected()){
				byte[] bytes = new byte[Double.BYTES];
				byte[] received = new byte[Integer.BYTES+Double.BYTES];
				ByteBuffer.wrap(bytes).putDouble(usignal);
				try {
					received = client.send(bytes);
				} catch (IOException e) {
					System.out.println("Failed to send control signal over pingserver");
					e.printStackTrace();
				}

				byte[] timeStamp = new byte[4];
				byte[] controlSignal = new byte[8];

				for(int k=0; k<Double.BYTES; k++){
					controlSignal[k] = received[4+k];
				}
				double cs = ByteBuffer.wrap(controlSignal).getDouble();
				mon.updateU(cs);
				for(int j=0; j<4; j++){
					timeStamp[4-1-j] = received[j];
				}

				double pingedDelay = (ByteBuffer.wrap(timeStamp).getInt())/1000000.0;
				mon.updateRealDelay(pingedDelay);
				if(pingedDelay < mon.getDelay()-0.010 || pingedDelay > mon.getDelay()+0.010){
					mon.updateDelay(estimateDelay(cs));
				}
			} else {
				mon.updateU(usignal);
			}
			
			updateState();
			period = mon.getParameters().H;
			t = (long) (t + period*1000);
			duration = t - System.currentTimeMillis();
			if(duration > 0){
				try {
					Thread.sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Calculates the controller output. */
	private double calculateOutput(double y, double yref) {
		pi = mon.getParameters();
		this.yref = yref;
		this.y = y;
		if(mon.getMode()==Monitor.SID){
			yHatDelayed = mon.getYHatDelayed(usat);
			ref2 = yref-y+yHatDelayed;
			yHat = mon.getYHat();
			u = pi.K*(ref2-yHat)+I;
			usat=sat(u,10,-10);
		}
		if (mon.getMode()==Monitor.DID){
			yHatDelayed = mon.getYHatDelayed(usat);
			ref2 = yref-y+yHatDelayed;
			yHat = mon.getYHat();
			u = pi.K*(ref2-yHat)+I;
			usat = sat(u,10,-10);
		}
		if(mon.getMode() == Monitor.OLD){
			u = pi.K*(yref-y)+I;
			usat=sat(u,10,-10);
		}
		return usat;
	}

	/**Updates the controller state*/
	private void updateState(){		
		if(pi.integratorOn){
			I=I+pi.K*pi.H/pi.Ti*(yref-y)+(pi.H/pi.Tr)*(usat-u);
		} else {
			I = 0.0;
		}
	}

	/**Saturates the control signal*/
	private double sat(double u, double umax, double umin){
		if(u>umax){
			return umax;
		}else if(u<umin){
			return umin;
		}
		return u;
	}

	/**Estimates the internet-delay*/
	private double estimateDelay(double controlSignal){
		mon.resetDelay();
		byte[] received = new byte[12];
		double accumulatedDelay=0.0;
		int iterations = 10;
		for (int i=0;i<iterations;i++){
			byte[] bytes = new byte[Double.BYTES];
			ByteBuffer.wrap(bytes).putDouble(usat);
			try {
				received = client.send(bytes);	// 12 bytes
			} catch (IOException e) {
				System.out.println("Sending failed");
				e.printStackTrace();
			}
			byte[] timeStamp = new byte[4];
			int ts=0;
			for(int j=0; j<4; j++){
				timeStamp[4-1-j] = received[j];
			}	
			ts = ByteBuffer.wrap(timeStamp).getInt();
			accumulatedDelay= accumulatedDelay + ts;
		}
		double estimatedDelay = (accumulatedDelay/iterations)/1000.0;	//ms
		System.out.println("A new estimated delay has been calculated to " + estimatedDelay + " [ms]");
		return 	estimatedDelay/1000;

	}


}
