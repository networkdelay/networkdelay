README


Communication over sockets

Setting up sockets for communication
	1. Open socket
	2. Open an input stream and an outputstream
	3. Read from and write to the stream according to the servers protocol
	4. Close the streams
	5. Close the sockets
	
Running the programs
	1. Start the server by 			//NOTE: Can be started both from local computer or computer on network (HOST)
		[*] Find out your ip or host name
			- >hostname
		* Compiling the .java file containing the server code
			>javac Server.java
		*Start with port as argument (ex "7")
			>java Server 7
	2. Start the client by opening a new shell and
		* Compiling the .java file containing the client code
			>javac Client.java
		* Start with address to host as argument as well as port
			>java Client "address to host or host name" 7
	3. Transmit strings from client by just typing and hitting return.
		* Should echo transmittion right back to client
	4. End transmittion by pressing Ctrl+Shift+C.
	
	*	Open cmd -> type ipconfig -> scroll up and find ipv4. -> your host ip is four sets of numbers seperated by dots.
	**	Java lessons about sockets : https://docs.oracle.com/javase/tutorial/networking/sockets/index.html
	***	For finding good cmd commands : https://en.wikipedia.org/wiki/Hostname